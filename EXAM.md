# CSCM602023 Advanced Programming (KKI) - 2020 - Final Programming Exam

> Azzahra Abraara - 1806173613

Task 1
- Fixed the exposed text analysis API key in application.properties
    1. add a new variable inside environment variable that contains the API key
        ![env variable](images/1.png)
    2. change the TEXT_ANALYTICS_API_KEY inside TextAnalysisServiceImpl into this:
        ![TEXT_ANALYTICS_API_KEY](images/2.png)
    3. and don't forget to change the application.properties into this:
        ![application.properties](images/3.png)

- add variables for database
        ![db2](images/5.PNG)
        ![db1](images/4.PNG)
        
- add log for all controller

Task 2
- if we view multiple conferences
    ![prometheus](images/6.PNG)